package de.aphol.java.tht.controller;

import javax.validation.Valid;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import de.aphol.java.tht.forms.LoginForm;
import de.aphol.java.tht.views.Views;

@Controller
public class PageController
{

    @GetMapping(value = {"/", "/index"})
    public String welcomePage(Model model, LoginForm loginForm)
    {
        model.addAttribute("pageTitle", "title.login");

        return Views.INDEX;
    }

    @PostMapping(value = {"/", "/index"})
    public String login(Model model, @Valid LoginForm loginForm, BindingResult bindingResult)
    {
        if (bindingResult.hasErrors()) {
            model.addAttribute("pageTitle", "title.login");
            return Views.INDEX;
        }
        
        model.addAttribute("pageTitle", "title.dashboard");
        return Views.DASHBOARD;
    }

    @GetMapping("/register")
    public String register(Model model)
    {
        model.addAttribute("pageTitle", "title.register");

        return Views.REGISTER;
    }

    @PostMapping("/register")
    public String validateRegistration(Model model)
    {
        model.addAttribute("pageTitle", "title.login");
        
        return Views.INDEX;
    }

}
