package de.aphol.java.tht.views;

public final class Views
{
    public static final String ERROR_404 = "error/error_404";
    public static final String ERROR_GENERIC = "error/error";

    public static final String INDEX = "index";
    public static final String REGISTER = "register";
    public static final String DASHBOARD = "dashboard";

    private Views()
    {
        /* Constant class, no instantiation allowed. */
    };
}
