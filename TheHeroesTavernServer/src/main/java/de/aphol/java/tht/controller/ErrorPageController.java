package de.aphol.java.tht.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import de.aphol.java.tht.views.Views;

@Controller
public class ErrorPageController implements ErrorController
{

    @RequestMapping(value = "/error")
    public String error(HttpServletRequest httpRequest, Model model)
    {
        int httpErrorCode = getErrorCode(httpRequest);

        switch (httpErrorCode)
        {
            case 404:
                model.addAttribute("pageTitle", "error.title");
                return Views.ERROR_404;
            default:
                model.addAttribute("pageTitle", "error.title");
                return Views.ERROR_GENERIC;
        }

    }

    @Override
    public String getErrorPath()
    {
        return "/error";
    }

    private int getErrorCode(HttpServletRequest httpRequest)
    {
        return (Integer) httpRequest.getAttribute("javax.servlet.error.status_code");
    }

}
