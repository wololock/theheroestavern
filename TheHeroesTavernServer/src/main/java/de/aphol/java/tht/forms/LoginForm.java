package de.aphol.java.tht.forms;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

public class LoginForm
{

    @NotEmpty(message = "{validation.mail.notEmpty}")
    @Email()
    private String mail;

    @NotEmpty()
    private String password;

    public String getMail()
    {
        return mail;
    }

    public String getPassword()
    {
        return password;
    }

    public void setMail(String mail)
    {
        this.mail = mail;
    }

    public void setPassword(String password)
    {
        this.password = password;
    }

    @Override
    public String toString()
    {
        return String.format("LoginForm[Username=%s, Password=######]", mail);
    }

}
